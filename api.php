<!-- Create a PHP file.

Inside of it, create a class named "API". Inside of the class, create a 3 function with parameters 
that execute printing the parameters you pass.

Outside of Class, use the class and call the function you created. 
In the 1st function, put your full name as the parameter of it.
In the 2nd Function, put an Array Value of your hobbies as the parameter of it.
In the 3rd Function, put an Object Value of your Age, email address and Birthday as the parameter of it.

If you have finished, commit & push it to your Project Repository -->



<?php
class API{
    public function getName($name){
        echo $name;
    }

    public function getHobbies($hobbies){
        print_r($hobbies);
    }

    public function getAll($all){
        print_r($all);
    }
}

$api = new API();

$api -> getName("Shuhaily Casan ");
$api -> getHobbies(['Basketball','Sleeping','Watching']);
$api -> getAll((Object) ["age" => '23', "email" => 'shuhailycasan@gmail.com',"birtdate"=>'1999-11-08']);

?>
